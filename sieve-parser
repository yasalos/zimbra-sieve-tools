#!/bin/bash

ERROR_SIEVE_TEXT_NOT_SPECIFIED=1
ERROR_RULE_NUM_NOT_SPECIFIED=2
ERROR_RULE_NUM_NOT_NUMBER=3
ERROR_RC_RULE_NAME_NOT_SPECIFIED=4
ERROR_RULE_NOT_FOUND=5

get_rc_rules_count(){
    if [ -z "$1" ]; then
        return 1
    fi
    echo "$1" | grep -c '# rule:\[.*\]\w*'
}



parse() {
    if [ -z "$1" ]; then
        return $ERROR_SIEVE_TEXT_NOT_SPECIFIED
    fi
    while read LINE; do
        let CURRENT_LINE++
        for TOKEN in $(echo "$LINE"); do
            if [[ $TOKEN =~ .*\{ ]]; then
                echo "$CURRENT_LINE:BS:$TOKEN"
                unset CURRENT_COMMAND
            elif [[ $TOKEN =~ .*\} ]]; then
                echo "$CURRENT_LINE:BE:$TOKEN"
                unset CURRENT_COMMAND
            elif [[ $TOKEN =~ \".* ]]; then
                P_STRING_STARTED=1
                echo "$CURRENT_LINE:S:$TOKEN"
            elif [[ $TOKEN =~ .*\" ]]; then
                unset P_STRING_STARTED
                echo "$CURRENT_LINE:S:$TOKEN"
            elif [ "$TOKEN" == "#" ]; then
                if [ -z $P_STRING_STARTED ]; then
                    echo "$CURRENT_LINE:c:$TOKEN"
                    COMMENT_LINE=1
                else
                    echo "$CURRENT_LINE:S:$TOKEN"
                fi
            elif [[ -z $CURRENT_COMMAND && -z $COMMENT_LINE ]]; then
                CURRENT_COMMAND=$TOKEN
                echo "$CURRENT_LINE:C:$CURRENT_COMMAND"
            else
                if [[ -z $P_STRING_STARTED ]]; then
                    echo "$CURRENT_LINE:P:$TOKEN"
                else
                    echo "$CURRENT_LINE:S:$TOKEN"
                fi
                if [[ $TOKEN =~ .*\; ]]; then
                    unset CURRENT_COMMAND
                fi
            fi
        done
        unset COMMENT_LINE
    done <<< "$1"
}

get_command() {    
    if [ -z "$1" ]; then
        return $ERROR_RULE_NUM_NOT_SPECIFIED
    fi
    
    if [ -z "$2" ]; then
        return $ERROR_SIEVE_TEXT_NOT_SPECIFIED
    fi
    
    if [[ $1 =~ [[:digit:]]* ]]; then
        OPENED_BLOCKS_COUNT=0
        OPENED_PARENTHESES_COUNT=0
        CURRENT_COMMAND_COUNT=0
        unset CURRENT_COMMAND
        unset OUTPUT
        unset STRING_STARTED
        while read LINE; do
            let CURRENT_LINE++
            
            TOKEN_SEPARATOR=$'\n'
            
            if [[ $LINE =~ ^$ && $CURRENT_COMMAND_COUNT -eq $1 ]]; then
                OUTPUT="${OUTPUT}${TOKEN_SEPARATOR}"
            fi
            
            for TOKEN in $(echo "$LINE"); do
                if [[ $TOKEN =~ .*\{ ]]; then
                    let OPENED_BLOCKS_COUNT++
                elif [[ $TOKEN =~ .*\} ]]; then
                    let OPENED_BLOCKS_COUNT--
                fi
                
                if [[ $TOKEN =~ .*\( ]]; then
                    let OPENED_PARENTHESES_COUNT++
                elif [[ $TOKEN =~ .*\) ]]; then
                    let OPENED_PARENTHESES_COUNT--
                fi
                
                if [[ $TOKEN =~ ^\" ]]; then
                    STRING_STARTED=1
                fi
                
                if [[ $TOKEN =~ .*\"\]*\;*\r* ]]; then
                    unset STRING_STARTED
                fi
                
                if [[ "$TOKEN" == "#" ]]; then
                    if [ -z $STRING_STARTED ]; then
                        break
                    fi
                elif [[ -z $CURRENT_COMMAND ]]; then
                    let CURRENT_COMMAND_COUNT++
                    CURRENT_COMMAND=$TOKEN
                fi
                
                if [ $CURRENT_COMMAND_COUNT -eq $1 ]; then
                    if [ -z "$OUTPUT" ]; then
                        # First line of the rule
                        if [[ "$PREVIOUS_LINE" =~ ^#.* ]]; then
                            OUTPUT="${PREVIOUS_LINE}"$'\n'"${TOKEN}"
                        else
                            OUTPUT="$TOKEN"
                        fi
                        TOKEN_SEPARATOR=" "
                    else
                        OUTPUT="${OUTPUT}${TOKEN_SEPARATOR}${TOKEN}"
                        TOKEN_SEPARATOR=" "
                    fi
                fi
                
                if [[ $OPENED_BLOCKS_COUNT -eq 0 ]]; then
                    if [[ $TOKEN =~ .*\} ]]; then
                        unset CURRENT_COMMAND
                        if [ $CURRENT_COMMAND_COUNT -eq $1 ]; then
                            # Stop parsing, beacause we already get the expected result.
                            echo "$OUTPUT"
                            return 0
                        fi
                    elif [[ $TOKEN =~ .*\; ]];then
                        if [ $OPENED_PARENTHESES_COUNT -eq 0 ]; then
                            unset CURRENT_COMMAND
                        else
                            continue
                        fi
                        if [ $CURRENT_COMMAND_COUNT -eq $1 ]; then
                            # Stop parsing, beacause we already get the expected result.
                            echo "$OUTPUT"
                            return 0
                        fi
                    fi
                    
                fi
                
                
                
                
            done
            
            PREVIOUS_LINE="$LINE"
        done <<< "$2"
        
    else
        return $ERROR_RULE_NUM_NOT_NUMBER
    fi
    
    return $ERROR_RULE_NOT_FOUND
    
}

has_vacation_command() {
    if [ -z "$1" ]; then
        return $ERROR_SIEVE_TEXT_NOT_SPECIFIED
    fi
    parse "$1" | grep -qE '[[:digit:]]*:C:vacation'
    return $?
}

has_redirect_command() {
    if [ -z "$1" ]; then
        return $ERROR_SIEVE_TEXT_NOT_SPECIFIED
    fi
    parse "$1" | grep -qE '[[:digit:]]*:C:redirect'
    return $?
}

get_rc_rule_name() {
    if [ -z "$1" ]; then
        return $ERROR_SIEVE_TEXT_NOT_SPECIFIED
    fi
    FIRST_LINE=$(echo "$1" | head -1)
    if [[ "$FIRST_LINE" =~ ^#[[:space:]]*.* ]]; then
        echo $FIRST_LINE | sed -e 's/^#[[:space:]]rule:\[//' | sed -e 's/\]\r*$//'
    else
        return $ERROR_RC_RULE_NAME_NOT_SPECIFIED
    fi
}
